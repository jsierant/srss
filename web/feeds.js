async function fetchFeeds(url) {
  const feeds_resp = await fetch(url)
  if (feeds_resp.status == 200) {
    return await feeds_resp.json()
  }
  return null
}

function createGroupName(name) {
  var groupName = document.createElement('h2')
  groupName.id = `group-name:${name}`
  groupName.className = 'group-name'
  groupName.value = name
  groupName.appendChild(document.createTextNode(name))
  return groupName
}

function createGroupBox(name) {
  console.log(`create group: ${name}`)
  var groupBox = document.createElement('div')
  groupBox.id = `group-box:${name}`
  groupBox.className = 'group-box';
  groupBox.appendChild(createGroupName(name))
  return groupBox
}

function createArticleTitle(title, link) {
  var a = document.createElement('a')
  a.href = link
  a.appendChild(document.createTextNode(title))

  var titleElement = document.createElement('h3')
  titleElement.className = 'article-title'
  titleElement.appendChild(a)
  return titleElement
}

function createArticleText(text) {
  var textNode = document.createTextNode(text)
  textNode.className = 'article-text'
  return textNode
}

function extractArticleSource(link) {
  var url = new URL(link);
  return url.hostname
}

function createArticleFootnote(date, source) {
  var footnote = document.createElement('div')
  footnote.className = 'article-footnote'
  footnote.appendChild(document.createTextNode(`${date} ${source}`))
  return footnote
}

function createArticle(article) {
  console.log(`create article: ${article["title"]}`)
  var artBox = document.createElement('div')
  artBox.className = 'article-box';
  artBox.appendChild(createArticleTitle(article['title'], article['link']))
  artBox.appendChild(createArticleText(article['text']))
  artBox.appendChild(
    createArticleFootnote(article['date'],
                          extractArticleSource(article['link'])))
  return artBox
}

async function setup() {
  const feeds_json = await fetchFeeds("feeds.json")
  if (!feeds_json) {
    console.log('Failed to download feeds')
  }
  for(const group in feeds_json) {
    var groupBox = createGroupBox(group)
    for(const article of feeds_json[group]) {
      groupBox.appendChild(createArticle(article))
    }
    document.body.appendChild(groupBox)
  }
}

setup()
