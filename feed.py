import feedparser
import sys
import yaml
import html
import dateutil.parser
import datetime
import re
import json


def get_text(entry):
    def remove_html_markup(s):
        tag = False
        quote = False
        out = ""
        for c in s:
            if c == '<' and not quote:
                tag = True
            elif c == '>' and not quote:
                tag = False
            elif (c == '"' or c == "'") and tag:
                quote = not quote
            elif not tag:
                out = out + c
        return html.unescape(out)

    if 'summary' in entry:
        return remove_html_markup(entry['summary'])
    if 'description' in entry:
        return remove_html_markup(entry['description'])


def update(group):
    def accept_article(article):
        if 'filter' not in group:
            return True
        return re.search(group['filter'], article['title'])\
            or re.search(group['filter'], article['text'])

    articles = []
    for source in group['sources']:
        feed = feedparser.parse(source['url'])
        print(source['url'])
        for entry in feed['entries']:
            article = {
                'title': entry['title'],
                'text': get_text(entry),
                'date': dateutil.parser.parse(entry['published']),
                'link': entry['link']
            }
            if accept_article(article):
                articles.append(article)
    return articles


def write_groups(out_file, groups):
    class DateTimeEncoder(json.JSONEncoder):
        def default(self, z):
            if isinstance(z, datetime.datetime):
                return (str(z))
            else:
                return super().default(z)

    with open(out_file, 'w') as gf:
        json.dump(groups, gf, cls=DateTimeEncoder, indent=2)


with open(sys.argv[1]) as f:
    config = yaml.load(f, Loader=yaml.FullLoader)
    groups = {}
    for gc in config['groups']:
        print('\n', gc['name'])
        groups[gc['name']] = update(gc)
    write_groups(sys.argv[2], groups)
